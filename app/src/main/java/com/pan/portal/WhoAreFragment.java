package com.pan.portal;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class WhoAreFragment extends Fragment {

    private View whoAreView = null;

    public WhoAreFragment() {
        // nil
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment

        /*if (this.whoAreView == null) {
            this.whoAreView = inflater.inflate(R.layout.activity_who_are_fragment, container, false);
        } else {
            ((ViewGroup) this.whoAreView.getParent()).removeView(this.whoAreView);
        }

        Log.i("WHOARE", this.whoAreView.toString());
        return this.whoAreView;*/
        return inflater.inflate(R.layout.activity_who_are_fragment, container, false);
    }


}
