package com.pan.portal;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;

import java.util.Arrays;
import java.util.List;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

/**
 * Created by pasqualeantonante on 06/08/16.
 */
public class QRscanner extends AppCompatActivity implements ZXingScannerView.ResultHandler {

    private static final String TAG = "QR-READER";
    private static final List<BarcodeFormat> SUPPORTED_FORMATS = Arrays.asList(BarcodeFormat.QR_CODE);
    private ZXingScannerView mScannerView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mScannerView = new ZXingScannerView(getApplicationContext());
        mScannerView.setResultHandler(this);
        mScannerView.setFormats(SUPPORTED_FORMATS);

        setContentView(mScannerView);
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScannerView.startCamera();          // Start camera on resume
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();           // Stop camera on pause
    }

    @Override
    public void handleResult(Result result) {

        Log.i(TAG, result.getBarcodeFormat().toString() + "->" + result.getText());

        if (result.getBarcodeFormat() != BarcodeFormat.QR_CODE) {
            Toast.makeText(this, R.string.non_qr_code_recognized, Toast.LENGTH_LONG);
            mScannerView.resumeCameraPreview(this);
            return;
        }

        // retrive interest
        Interest i = ((ApplicationState) getApplication()).getInterestById(result.getText());
        if (i != null) {
            Intent toPano = new Intent(this, PanoramaActivity.class);
            toPano.putExtra("INTEREST", i);
            startActivity(toPano);
        } else {
            Toast.makeText(this, R.string.invalid_qr_code, Toast.LENGTH_LONG);
            mScannerView.resumeCameraPreview(this);
        }

        return;
    }
}
