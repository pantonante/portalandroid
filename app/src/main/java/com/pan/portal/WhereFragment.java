package com.pan.portal;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.pan.portal.datamodel.InterestChangeListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class WhereFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnInfoWindowClickListener {

    private static final String TAG = "MAP";
    private static final int GPS_PERMISSION = 2;

    private static final int DEFAULT_ZOOM = 8;

    private LocationManager locationManager;
    private Location location = null;
    private GoogleMap mMap;
    private Boolean mapReady = false;

    private Map<Marker, Interest> markerMap = new HashMap<Marker, Interest>();

    public WhereFragment() {
        // nil
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.activity_where_fragment, container, false);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) this.getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        return v;
    }


    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == GPS_PERMISSION) {
            if (ActivityCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                if (mapReady) {
                    mMap.setMyLocationEnabled(true);
                    locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
                    location = locationManager.getLastKnownLocation(locationManager.getBestProvider(new Criteria(), false));
                }
            } else {
                Toast.makeText(getContext(), "GPS permission required", Toast.LENGTH_LONG).show();
            }
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mapReady = true;
        //mMap.setOnMarkerClickListener(this);
        mMap.setOnInfoWindowClickListener(this);

        mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() { // customize interest snippet
            @Override
            public View getInfoWindow(Marker marker) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
                View snippetView = getActivity().getLayoutInflater().inflate(R.layout.interest_marker_snippet, null);
                TextView snippetTitle = ((TextView) snippetView.findViewById(R.id.snippet_title));
                snippetTitle.setText(marker.getTitle());
                TextView snippetSubtitle = ((TextView) snippetView.findViewById(R.id.snippet_subtitle));
                snippetSubtitle.setText(marker.getSnippet());
                ImageView goToPano = (ImageView) snippetView.findViewById(R.id.go_to_pano);

                return snippetView;
            }
        });

        // Check and enable FINE_LOCATION
        if (ContextCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
            locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
            location = locationManager.getLastKnownLocation(locationManager.getBestProvider(new Criteria(), false));
        } else {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, GPS_PERMISSION);
        }

        // Add a markers
        ArrayList<Interest> interests = ((ApplicationState) getActivity().getApplication()).getInterests();
        if (interests != null)
            addAllInterestToMap(interests);

        ((ApplicationState) getActivity().getApplication()).addInterestChangeListener(new InterestChangeListener() {
            @Override
            public void stateChanged(ArrayList<Interest> interests) {
                addAllInterestToMap(interests);
            }
        });

        // set camera to local position
        if (location != null) {
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), DEFAULT_ZOOM));

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(location.getLatitude(), location.getLongitude()))      // Sets the center of the map to location user
                    .zoom(DEFAULT_ZOOM)                   // Sets the zoom
                    //.bearing(90)              // Sets the orientation of the camera to east
                    .tilt(40)                   // Sets the tilt of the camera to 40 degrees
                    .build();                   // Creates a CameraPosition from the builder
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        } /*else {
            mMap.moveCamera(CameraUpdateFactory.newLatLng(???));
        }*/
    }

    /*@Override
    public boolean onMarkerClick(Marker marker) {
        Interest interest = markerMap.get(marker);
        Intent toPano = new Intent(getContext(),PanoramaActivity.class);
        toPano.putExtra("INTEREST", interest);
        getContext().startActivity(toPano);
        return false;
    }*/

    @Override
    public void onInfoWindowClick(Marker marker) {
        Interest interest = markerMap.get(marker);
        Intent toPano = new Intent(getContext(), PanoramaActivity.class);
        toPano.putExtra("INTEREST", interest);
        getContext().startActivity(toPano);
    }

    private void addAllInterestToMap(ArrayList<Interest> interests) {
        for (Interest i : interests) {
            LatLng ll = new LatLng(i.getLat(), i.getLon());
            Marker m = mMap.addMarker(new MarkerOptions()
                    .position(ll)
                    .title(i.getTitle())
                    .snippet(i.getSubtitle()));
            markerMap.put(m, i);
            //.icon(BitmapDescriptorFactory.fromResource(R.drawable.duotone_locate)
        }
    }
}