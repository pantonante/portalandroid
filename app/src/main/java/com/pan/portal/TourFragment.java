package com.pan.portal;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pan.portal.datamodel.InterestChangeListener;

import java.util.ArrayList;

public class TourFragment extends Fragment {

    RecyclerView rv;

    public TourFragment() {
        // nil
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View inflatedView = inflater.inflate(R.layout.activity_tour_fragment, container, false);

        rv = (RecyclerView) inflatedView.findViewById(R.id.card_recycler);
        rv.setHasFixedSize(true);

        LinearLayoutManager llm = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        rv.setLayoutManager(llm);

        //rv.addItemDecoration(new DividerItemDecoration(ContextCompat.getDrawable(this,R.drawable.line_divider),false,false));

        ApplicationState as = (ApplicationState) getActivity().getApplication();
        as.addInterestChangeListener(new InterestChangeListener() {
            @Override
            public void stateChanged(ArrayList<Interest> interests) {
                InterestCardAdapter adapter = new InterestCardAdapter(interests, getContext());
                rv.setAdapter(adapter);
                rv.invalidate();
            }
        });
        InterestCardAdapter adapter = new InterestCardAdapter(as.getInterests(), getContext());
        rv.setAdapter(adapter);

        return inflatedView;
    }
}
