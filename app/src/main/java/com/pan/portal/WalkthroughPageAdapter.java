package com.pan.portal;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by pasqualeantonante on 04/08/16.
 */
public class WalkthroughPageAdapter extends PagerAdapter {
    private Integer[] pageImagesIds = {R.drawable.splash, R.drawable.zen, R.drawable.share};
    private String[] pageHeaders = {"Benvenuto", "Scopri", "Condividi"};
    private String[] pageDescriptions = {"Visita le nostre strutture", "I posti più belli", "I tuoi momenti più belli"};

    private LayoutInflater inflater;
    private Context context;


    public WalkthroughPageAdapter(Context context) {
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return pageImagesIds.length;
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View walkthroughLayout = inflater.inflate(R.layout.walkthrough_layout, view, false);

        final ImageView imageView = (ImageView) walkthroughLayout.findViewById(R.id.walkthrough_image);
        imageView.setImageResource(pageImagesIds[position]);

        final TextView header = (TextView) walkthroughLayout.findViewById(R.id.walkthrough_header);
        header.setText(pageHeaders[position]);

        final TextView description = (TextView) walkthroughLayout.findViewById(R.id.walkthrough_description);
        description.setText(pageDescriptions[position]);

        final Button joinButton = (Button) walkthroughLayout.findViewById(R.id.joinBtn);
        joinButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                context.startActivity(new Intent(context, LoginActivity.class));
            }
        });

        view.addView(walkthroughLayout, 0);

        return walkthroughLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }

}
