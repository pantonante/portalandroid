package com.pan.portal;

import android.app.Application;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.pan.portal.datamodel.InterestChangeListener;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * Created by pasqualeantonante on 08/08/16.
 */
public class ApplicationState extends Application {

    private static final String TAG = "APP-STATE";

    private HashSet<InterestChangeListener> onChangeListener;

    private ArrayList<Interest> interests;
    private FirebaseDatabase database;
    private DatabaseReference interestRef;

    @Override
    public void onCreate() {
        super.onCreate();
        //interests = Interest.demoInterest();
        onChangeListener = new HashSet<>();
        interests = new ArrayList<>();
    }

    public ArrayList<Interest> getInterests() {
        return interests;
    }

    public Interest getInterestById(String id) {
        Boolean found = false;
        Interest reqInterest = null;
        for (Interest i : interests) {
            if (i.getId().equalsIgnoreCase(id)) {
                reqInterest = i;
                break;
            }
        }
        return reqInterest;
    }

    public boolean connectToDatabase() {
        database = FirebaseDatabase.getInstance();
        if (database != null) {
            interestRef = database.getReference().child("interests");
            interestRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Log.i(TAG, "# of interests: " + dataSnapshot.getChildrenCount());
                    for (DataSnapshot interestSnapshot : dataSnapshot.getChildren()) {
                        Interest tmpInterests = interestSnapshot.getValue(Interest.class);
                        interests.add(tmpInterests);
                        Log.i(TAG, tmpInterests.getTitle() + "->" + tmpInterests.getThumbnailUrl());
                    }
                    interestUpdated();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
            return true;
        } else {
            return false;
        }
    }

    public synchronized void addInterestChangeListener(InterestChangeListener interestChangeListener) {
        if (interestChangeListener != null) this.onChangeListener.add(interestChangeListener);
    }

    public synchronized void removeInterestChangeLIstener(InterestChangeListener interestChangeListener) {
        if (interestChangeListener != null) this.onChangeListener.remove(interestChangeListener);
    }

    private void interestUpdated() {
        for (InterestChangeListener listener : onChangeListener)
            listener.stateChanged(this.getInterests());
    }
}
