package com.pan.portal;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by pasqualeantonante on 05/08/16.
 */
public class InterestCardAdapter extends RecyclerView.Adapter<InterestCardAdapter.CardHolder> {

    ArrayList<Interest> interests;
    Context context;

    public InterestCardAdapter(ArrayList<Interest> interests, Context context) {
        super();
        this.interests = interests;
        this.context = context;
    }

    @Override
    public InterestCardAdapter.CardHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.interest_card, parent, false);
        CardHolder ch = new CardHolder(v);
        return ch;
    }

    @Override
    public void onBindViewHolder(InterestCardAdapter.CardHolder holder, int position) {
        holder.title.setText(interests.get(position).getTitle());

        //https://www.dropbox.com/s/mnb4nbfh48pljoo/ambras_thumb.jpg
        //holder.thumbnail.setImageResource(interests.get(position).getFeaturedImageId());

        Picasso.with(this.context)
                .load(interests.get(position).getThumbnailUrl())
                .into(holder.thumbnail);

        holder.interest = interests.get(position);
    }

    @Override
    public int getItemCount() {
        if (interests == null || interests.size() == 0)
            return 0;
        else
            return interests.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public static class CardHolder extends RecyclerView.ViewHolder {
        CardView cv;
        TextView title;
        ImageView thumbnail;
        Interest interest;

        CardHolder(View itemView) {
            super(itemView);
            cv = (CardView) itemView.findViewById(R.id.interest_card);
            cv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent toPano = new Intent(view.getContext(), PanoramaActivity.class);
                    toPano.putExtra("INTEREST", interest);
                    view.getContext().startActivity(toPano);
                }
            });
            title = (TextView) itemView.findViewById(R.id.interest_title);
            thumbnail = (ImageView) itemView.findViewById(R.id.interest_thumbnail);
        }
    }

}
