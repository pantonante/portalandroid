package com.pan.portal.datamodel;

import com.pan.portal.Interest;

import java.util.ArrayList;

/**
 * Created by pasqualeantonante on 09/08/16.
 */
public interface InterestChangeListener {
    void stateChanged(ArrayList<Interest> interests);
}
