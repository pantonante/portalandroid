package com.pan.portal;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by pasqualeantonante on 05/08/16.
 */
public class Interest implements Parcelable {
    public static final Creator<Interest> CREATOR = new Creator<Interest>() {
        @Override
        public Interest createFromParcel(Parcel source) {
            return new Interest(source);
        }

        @Override
        public Interest[] newArray(int size) {
            return new Interest[size];
        }
    };

    private String id = "";
    private String title = "";
    private String subtitle = "";
    private String caption = "";
    private String description = "";
    private Double lat;
    private Double lon;
    private String url = "";
    private String thumbnailUrl = "";
    private String panoUrl = "";

    public Interest() {
        return;
    }

    /*public static final Interest[] demoInterest(){
        return new Interest[]{
                new Interest("p1", "Castello di Ambras",
                                  "Innsbruck, Austria","Sala spagnola",
                        "Il Castello di Ambras (in tedesco Schloss Ambras) si trova ad Innsbruck, Austria. Situato nelle colline presso la città austriaca, il castello ne costituisce una delle più importanti attrazioni. La sua importanza storico-culturale è fortemente legata all'arciduca Ferdinando II.\n\nCome molti edifici europei, il Castello di Ambras porta una firma italiana, quella dell'architetto scaligero Giovanni Battista Guarienti. Originario di una delle più antiche famiglie della nobiltà veronese, il Guarienti era giunto alla corte dell'arciduca Ferdinando II, in Boemia, con la qualifica di sovrintendente dei palazzi che il figlio dell'imperatore possedeva nei paesi dell'Est europeo: Ungheria, Polonia, Cecoslovacchia.",
                        R.drawable.ambras_thumb, "ambras_pano.jpg",
                        47.256840, 11.433909,
                        "https://it.wikipedia.org/wiki/Castello_di_Ambras"),
                new Interest("p2", "Terrazza Mascagni",
                        "Livorno","Vista dal moletto",
                        "La Terrazza Mascagni è uno dei luoghi più eleganti e suggestivi di Livorno ed è ubicata sul lungomare a margine del viale Italia.\n\nNell'area occupata da questo belvedere, un tempo sorgeva un fortilizio facente parte del sistema difensivo della costa. Noto come Forte dei Cavalleggeri, era composto da una torre e da un vasto complesso edilizio; occupava un'area di 30 x 60 metri e la torre, posta all'estremità ovest, era formata da tre piani fuori terra. Nel forte alloggiava un distaccamento dei cavalleggeri per il pattugliamento della costa finalizzato soprattutto ad impedire il contrabbando e a garantire la sicurezza sanitaria degli sbarchi. Dopo l'Unità d'Italia, la costruzione fu ceduta al Comune e fu smantellata nel 1872.[1], compresa la torre.\n\nNell'ultimo decennio dell'Ottocento qui si registra la costruzione di un parco di divertimenti, l'Eden, che rimase in funzione fino ai primi anni del secolo successivo; in questa struttura, sin dal 1896, si tennero alcuni dei primi spettacoli cinematografici italiani.\n\nLa trasformazione della spianata in una grande piazza sul mare avvenne solo a partire dal 1925 su progetto dell'ingegner Enrico Salvais con la collaborazione di Luigi Pastore. I lavori furono conclusi rapidamente e in seguito, nei primi anni trenta, Ghino Venturi vi edificò il Gazebo per la musica (donato da Pedro Bossio), un tempietto rotondo con una calotta sorretta da colonne circolari, successivamente distrutto dai bombardamenti della seconda guerra mondiale. La Terrazza fu quindi intitolata a Costanzo Ciano, livornese e figura di spicco del Partito Fascista, nonché padre di Galeazzo. Il fatto deve leggersi nel quadro della retorica del regime; infatti, malgrado non possa ritenersi artifice della realizzazione dell'opera, con grande abilità Ciano fece sì che il proprio nome fosse accostato a quello di questa ed altre strutture pubbliche.\n\nNel dopoguerra fu notevolmente ampliata verso nord utilizzando le macerie del centro cittadino distrutto dai bombardamenti e venne pertanto dedicata al compositore livornese Pietro Mascagni.\n\nGravemente danneggiata nel corso degli anni dalle violente mareggiate e dall'incuria, sul finire degli anni novanta la Terrazza è stata completamente restaurata, con il ripristino inoltre delle aree verdi circostanti e con la fedele ricostruzione dello stesso Gazebo.",
                        R.drawable.mascagni_thumb, "terrazza_pano.jpg",
                        43.535438, 10.299903,
                        "https://it.wikipedia.org/wiki/Terrazza_Mascagni"),
                new Interest("p3", "Palouse Falls",
                        "Washington, United States","Vista dal basso della cascata",
                        "Quelle del fiume Palouse sono tra le cascate più belle d’America. Più alte delle più note Cascate del Niagare, si gettano da un’altezza di circa 60 metri, confluendo in una piscina da brividi.\n\nCuriosità\nUn record che fa venire davvero i brividi, una caduta libera in kayak di 60 metri dalle cascate Palouse. Un volo che ha davvero dell’incredibile e che è stato effettuato da Tyler Bradt. Il portentoso kayaker riporta lievi ferite e un polso slogato.",
                        R.drawable.palaouse_thumb, "palousefall_pano.jpg",
                        46.663302, -118.223963,
                        "https://it.wikipedia.org/wiki/Palouse_Falls"),
                new Interest("p4", "Tallinn",
                        "Estonia", "Lago con cigni",
                        "Tallinn (Reval in tedesco, Tallinna in finlandese) è la capitale dell'Estonia nonché suo principale porto, è situata nella costa settentrionale del paese, affacciata sul Mar Baltico, in linea d'aria è divisa da 80 chilometri di Mar Baltico da Helsinki, quest'ultima situata più a Nord, inoltre Tallin è anche la città più popolosa e maggiore centro economico e commerciale del paese estone.\n\nLa sua Città Vecchia medioevale, antico porto anseatico, è divenuta patrimonio dell'umanità dell'UNESCO nel 1997.\nTallinn è stata la Capitale Europea della Cultura per l'anno 2011 assieme alla città finlandese di Turku.",
                        R.drawable.tallinn_thumb, "tallinn_pano.jpg",
                        59.417199, 24.777375,
                        "https://it.wikipedia.org/wiki/Tallinn"),
                new Interest("p5","Resort Maiami",
                        "5907 Pine Tree Drive, Miami Beach, FL", "Giardino",
                        "Goto the living room and stary warm by the fire! This beautiful 1929 home has kept the integrity of the 20’s with its Florida Pine hardwood floor and original Cuban tiles. The two story home offers spacious spaces including a Florida Room with water view, 2 dining rooms, 4 bedrooms, 4 bathrooms, fully equipped kitchen, washer/dryer, and poolside BBQ. It also includes a comfortable living room with flat screen TV, DVD player, Hi-Fi multimedia system, and Wireless internet access. This very private villa is located minutes away from the heart of South Beach in the famous 'Millionaire’s Row' and was occupied by celebrities such as Lenny Kravitz. Great proximity to the beach, supermarkets, parks, restaurants… This is a great home for your Miami Beach vacation rental.  (Description from website MiaVac.com)",
                        R.drawable.miami_pool_thumb, "miami_pool_pano.jpg",
                        25.839695, -80.123734,
                        "www.google.it"),
        };
    }*/


    public Interest(String id, String title, String subtitle, String caption, String descr, String thumbnailUrl, String panoUrl, Double lat, Double lon, String url) {
        this.id = id;
        this.title = title;
        this.subtitle = subtitle;
        this.caption = caption;
        this.description = descr;
        this.thumbnailUrl = thumbnailUrl;
        this.panoUrl = panoUrl;
        this.lat = lat;
        this.lon = lon;
        this.url = url;
    }

    protected Interest(Parcel in) {
        this.id = in.readString();
        this.title = in.readString();
        this.subtitle = in.readString();
        this.caption = in.readString();
        this.description = in.readString();
        this.lat = (Double) in.readValue(Double.class.getClassLoader());
        this.lon = (Double) in.readValue(Double.class.getClassLoader());
        this.url = in.readString();
        this.thumbnailUrl = in.readString();
        this.panoUrl = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.title);
        dest.writeString(this.subtitle);
        dest.writeString(this.caption);
        dest.writeString(this.description);
        dest.writeValue(this.lat);
        dest.writeValue(this.lon);
        dest.writeString(this.url);
        dest.writeString(this.thumbnailUrl);
        dest.writeString(this.panoUrl);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public String getPanoUrl() {
        return panoUrl;
    }

    public void setPanoUrl(String panoUrl) {
        this.panoUrl = panoUrl;
    }
}
