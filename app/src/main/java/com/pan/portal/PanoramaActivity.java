package com.pan.portal;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewManager;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.vr.sdk.widgets.pano.VrPanoramaEventListener;
import com.google.vr.sdk.widgets.pano.VrPanoramaView;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

/**
 * Created by pasqualeantonante on 06/08/16.
 */
public class PanoramaActivity extends Activity {
    private static final String TAG = "PANO";
    /**
     * Arbitrary variable to track load status. In this example, this variable should only be accessed
     * on the UI thread. In a real app, this variable would be code that performs some UI actions when
     * the panorama is fully loaded.
     */
    public boolean loadImageSuccessful;
    private TextView panoTitle;
    private TextView panoSubtitle;
    private VrPanoramaView panoWidgetView;
    private TextView panoCaption;
    private TextView panoDescription;
    private Interest interest;
    /**
     * Tracks the file to be loaded across the lifetime of this app.
     **/
    private Uri fileUri;

    /**
     * Configuration information for the panorama.
     **/
    private VrPanoramaView.Options panoOptions = new VrPanoramaView.Options();
    private Target backgroundImageLoaderTarget;

    /**
     * Called when the app is launched via the app icon or an intent using the adb command above. This
     * initializes the app and loads the image to render.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pano_layout);

        Intent intent = getIntent();
        interest = (Interest) intent.getParcelableExtra("INTEREST");

        panoTitle = (TextView) findViewById(R.id.pano_title);
        panoTitle.setText(interest.getTitle());
        panoSubtitle = (TextView) findViewById(R.id.pano_subtitle);
        panoSubtitle.setText(interest.getSubtitle());
        panoWidgetView = (VrPanoramaView) findViewById(R.id.pano_view);
        panoWidgetView.setEventListener(new ActivityEventListener());

        // Remove useless ImageButtons
        final ImageButton gvr_settings = (ImageButton) panoWidgetView.findViewById(R.id.ui_settings_icon);
        ((ViewManager) gvr_settings.getParent()).removeView(gvr_settings);

        final ImageButton gvr_info = (ImageButton) panoWidgetView.findViewById(R.id.info_button);
        ((ViewManager) gvr_info.getParent()).removeView(gvr_info);
        // ---

        panoCaption = (TextView) findViewById(R.id.pano_view_caption);
        panoCaption.setText(interest.getCaption());

        panoDescription = (TextView) findViewById(R.id.pano_description);
        panoDescription.setText(interest.getDescription());

        if (backgroundImageLoaderTarget != null)
            Picasso.with(getApplicationContext()).cancelRequest(backgroundImageLoaderTarget);

        // Initial launch of the app or an Activity recreation due to rotation.
        handleIntent(getIntent());
    }

    /**
     * Called when the Activity is already running and it's given a new intent.
     */
    @Override
    protected void onNewIntent(Intent intent) {
        // Save the intent. This allows the getIntent() call in onCreate() to use this new Intent during
        // future invocations.
        setIntent(intent);
        // Load the new image.
        handleIntent(intent);
    }

    /**
     * Load custom images based on the Intent or load the default image. See the Javadoc for this
     * class for information on generating a custom intent via adb.
     */
    private void handleIntent(Intent intent) {
        // Determine if the Intent contains a file to load.
        if (Intent.ACTION_VIEW.equals(intent.getAction())) {
            Log.i(TAG, "ACTION_VIEW Intent recieved");

            fileUri = intent.getData();
            if (fileUri == null) {
                Log.w(TAG, "No data uri specified. Use \"-d /path/filename\".");
            } else {
                Log.i(TAG, "Using file " + fileUri.toString());
            }

            panoOptions.inputType = intent.getIntExtra("inputType", VrPanoramaView.Options.TYPE_MONO);
            Log.i(TAG, "Options.inputType = " + panoOptions.inputType);
        } else {
            Log.i(TAG, "Intent is not ACTION_VIEW. Using default pano image.");
            fileUri = null;
            panoOptions.inputType = VrPanoramaView.Options.TYPE_MONO;
        }

        backgroundImageLoaderTarget = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                panoWidgetView.loadImageFromBitmap(bitmap, panoOptions);
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        };
        Picasso.with(getApplicationContext()).load(interest.getPanoUrl()).into(backgroundImageLoaderTarget);
    }

    @Override
    protected void onPause() {
        panoWidgetView.pauseRendering();
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        panoWidgetView.resumeRendering();
    }

    @Override
    protected void onDestroy() {
        // Destroy the widget and free memory.
        panoWidgetView.shutdown();
        if (backgroundImageLoaderTarget != null)
            Picasso.with(getApplicationContext()).cancelRequest(backgroundImageLoaderTarget);
        super.onDestroy();
    }

    public void recursiveLoopChildren(ViewGroup parent) {
        for (int i = parent.getChildCount() - 1; i >= 0; i--) {
            final View child = parent.getChildAt(i);

            if (child != null) {
                Integer id = child.getId();
                Log.i(TAG, child.toString() + "--->" + id);

                if (child instanceof ViewGroup) {
                    recursiveLoopChildren((ViewGroup) child);
                }
            }
        }
    }

    /**
     * Listen to the important events from widget.
     */
    private class ActivityEventListener extends VrPanoramaEventListener {
        /**
         * Called by pano widget on the UI thread when it's done loading the image.
         */
        @Override
        public void onLoadSuccess() {
            loadImageSuccessful = true;
        }

        /**
         * Called by pano widget on the UI thread on any asynchronous error.
         */
        @Override
        public void onLoadError(String errorMessage) {
            loadImageSuccessful = false;
            Toast.makeText(
                    PanoramaActivity.this, "Error loading pano: " + errorMessage, Toast.LENGTH_LONG)
                    .show();
            Log.e(TAG, "Error loading pano: " + errorMessage);
        }

        @Override
        public void onDisplayModeChanged(int newDisplayMode) {
            super.onDisplayModeChanged(newDisplayMode);
            Log.i(TAG, "GVR DisplayMode: " + newDisplayMode);
        }
    }
}
